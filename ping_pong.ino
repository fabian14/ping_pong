#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#define PIN_SCHLAEGER_A   A0
#define PIN_SCHLAEGER_B   A0 //A1

#define PIXEL_BREITE      127 //X
#define PIXEL_HOEHE       31 //Y, oder 63
#define SCHLAEGER_BREITE  2
#define SCHLAEGER_HOEHE   8
#define ABST_SCHL_WAND    10 //Abstand Schläger von Wand
#define BALL_GROESSE      2 //min 2 für Algo

#define PUNKTE_SPIELENDE  15
#define MAX_X_GESCHW      2 //auf alle Fälle 2!!!
#define MAX_Y_GESCHW      2


Adafruit_SSD1306 display(-1); //-1 kein Reset PIN vorhanden

//Initialisierungen
int schlAPos = 0;
int schlBPos = 0;

int XPosBall;
int YPosBall;
int XGeschw;
int YGeschw;

int punkteA;
int punkteB;

bool startSpiel = true;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);

  display.begin(SSD1306_SWITCHCAPVCC, 0x3C); //Adresse für 128x64 Pixel Display
  //Uno, Ethernet  A4 (SDA), A5 (SCL)
  display.clearDisplay();
  printText("Ping Pong", 0, PIXEL_HOEHE / 2 - 3 * 2, 2);
  display.display();
  delay(500);
}

void loop() {
  // put your main code here, to run repeatedly:
  SpielIni();
  while (startSpiel)
  {
    SchlBallBerechnen();
    zeichnen();
  }

  delay(5000);
  startSpiel = true;
}

void SpielIni()
{
  punkteA = 0;
  punkteB = 0;
  zweiteIni();
}

void zweiteIni()
{
  XPosBall = PIXEL_BREITE / 2;
  YPosBall = random(MAX_Y_GESCHW, PIXEL_HOEHE - MAX_Y_GESCHW);
  XGeschw = random(1, MAX_X_GESCHW);
  do
  {
    YGeschw = random(-MAX_Y_GESCHW, MAX_Y_GESCHW);
  } while (YGeschw == 0);
}


void SchlBallBerechnen()
{
  schlAPos = map(analogRead(PIN_SCHLAEGER_A), 0, 1023, 0, PIXEL_HOEHE - SCHLAEGER_HOEHE);
  schlBPos = map(analogRead(PIN_SCHLAEGER_B), 0, 1023, 0, PIXEL_HOEHE - SCHLAEGER_HOEHE);
  //  String s;
  //  s = "Pos X: ";
  //  s += schlAPos;
  //  s += "  Pos Y: ";
  //  s += schlBPos;
  //  Serial.println(s);

  XPosBall += XGeschw;
  YPosBall += YGeschw;

  // Gegen obere oder untere Wand
  if (YPosBall >= PIXEL_HOEHE - BALL_GROESSE || YPosBall <= 0)
  {
    YGeschw *= -1;
  }
  // gegen linken Schläger
  if ((XPosBall == ABST_SCHL_WAND || XPosBall == ABST_SCHL_WAND + BALL_GROESSE - 1) && XGeschw < 0)
  {
    if (YPosBall > schlAPos - BALL_GROESSE + 1 && YPosBall < schlAPos + SCHLAEGER_HOEHE)
    {
      XGeschw *= -1;
    }
  }

  //gegen rechten Schläer
  if ((XPosBall == PIXEL_BREITE - ABST_SCHL_WAND - SCHLAEGER_BREITE || XPosBall == PIXEL_BREITE - ABST_SCHL_WAND - BALL_GROESSE + 1) && XGeschw > 0)
  {
    if (YPosBall > schlBPos - BALL_GROESSE + 1 && YPosBall < schlBPos + SCHLAEGER_HOEHE)
    {
      XGeschw *= -1;
    }
  }

  // Punkt gemacht, gegen linke oder rechte Wand

  if (XPosBall <= 0)
  {
    punkteB++;
    zweiteIni();
    XGeschw *= -1;
  }
  else if (XPosBall >= PIXEL_BREITE - BALL_GROESSE)
  {
    punkteA++;
    zweiteIni();
  }
  if (punkteA >= PUNKTE_SPIELENDE || punkteB >= PUNKTE_SPIELENDE)
  {
    startSpiel = false;
  }
}


void zeichnen()
{
  display.clearDisplay();
  //Punltestand immer zeichnen!
  printText(String(punkteA), (PIXEL_BREITE / 4) - 6, 0, 1);
  printText(String(punkteB), (PIXEL_BREITE * 3 / 4) - 6, 0, 1);

  if (punkteA >= PUNKTE_SPIELENDE)
  {
    printText("Spieler A gewinnt! :D", 0, PIXEL_HOEHE / 2, 1);

  } else if (punkteB >= PUNKTE_SPIELENDE)
  {
    printText("Spieler B gewinnt! :D", 0, PIXEL_HOEHE / 2, 1);
  } else
  {
    // Schläger zeichnen
    display.fillRect(ABST_SCHL_WAND - SCHLAEGER_BREITE, schlAPos, SCHLAEGER_BREITE, SCHLAEGER_HOEHE, WHITE);
    display.fillRect(PIXEL_BREITE - ABST_SCHL_WAND + SCHLAEGER_BREITE, schlBPos, SCHLAEGER_BREITE, SCHLAEGER_HOEHE, WHITE);
    // Mittellinie zeichnen
    display.drawFastVLine(PIXEL_BREITE / 2, 0, PIXEL_HOEHE, WHITE);
    // Ball zeichnen
    display.fillRect(XPosBall, YPosBall, BALL_GROESSE, BALL_GROESSE, WHITE);
  }
  display.display();
}


void printText(String s, int posX, int posY, int groesse)
{
  display.setTextSize(groesse);
  display.setTextColor(WHITE);
  display.setCursor(posX, posY);
  display.print(s);
}

